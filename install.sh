#!/bin/bash
EXTID="bjdchcdfhdciemcopdjnbfaggggiiakp"
echo "Trying to install crafta-adapter google chrome extension..."
if [[ "$OSTYPE" == "linux-gnu" ]]; then
  FILE="$HOME/.config/google-chrome/NativeMessagingHosts/com.add0n.node.json"
elif [[ "$OSTYPE" == "darwin"* ]]; then
  FILE="$HOME/Library/Application Support/Google/Chrome/NativeMessagingHosts/com.add0n.node.json"
  #brew install moreutils jq
else
  echo "Your os is not supported."; exit 1;
fi
echo "Extension id - $EXTID"
[ ! -f "$FILE" ] && { echo "Native Client config file not found."; exit 1; }
if grep -iq "$EXTID" "$FILE"; then
  echo "Extension already installed."
  exit 1
fi
jq --arg entry "chrome-extension://$EXTID/" '.allowed_origins += [$entry]' "$FILE" | sponge "$FILE"
echo "Success!"