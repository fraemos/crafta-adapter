import "../css/options.css"
import {config} from './common/config'

const notify = msg => {
  const status = document.getElementById('status');
  window.clearTimeout(notify.id);
  notify.id = window.setTimeout(() => status.textContent = '', 2000);
  status.textContent = msg;
};

function save() {
  chrome.storage.local.set({
    noiseLevel: document.getElementById('noise-level').value,
    imageQuality: document.getElementById('image-quality').value,
    forceOpenCL: document.getElementById('force-opencl').checked,
    disableGpu: document.getElementById('disable-gpu').checked,
    outputDir: document.getElementById('output-dir').value.trim(),
  }, () => {
    notify('Настройки сохранены.');
  });
}

function restore() {
  chrome.storage.local.get({
    noiseLevel: config.noiseLevel,
    imageQuality: config.imageQuality,
    forceOpenCL: config.forceOpenCL,
    disableGpu: config.disableGpu,
    outputDir: config.outputDir
  }, prefs => {
    document.getElementById('noise-level').value = prefs.noiseLevel;
    document.getElementById('image-quality').value = prefs.imageQuality;
    document.getElementById('force-opencl').checked = prefs.forceOpenCL;
    document.getElementById('disable-gpu').checked = prefs.disableGpu;
    document.getElementById('output-dir').value = prefs.outputDir;
  });
}
document.addEventListener('DOMContentLoaded', restore);
document.getElementById('save').addEventListener('click', save);

document.getElementById('reset').addEventListener('click', e => {
  if (e.detail === 1) {
    notify('Двойной клик для сброса!');
  }
  else {
    localStorage.clear();
    chrome.storage.local.clear(() => {
      chrome.runtime.reload();
      window.close();
    });
  }
});

