export const config = {
  noiseLevel: 3,
  imageQuality: 100,
  forceOpenCL: false,
  disableGpu: false,
  outputDir: "",
};

export const processStateDefaults = {
  productsTotal: 0,
  productCurrent: 0,
  productCurrentSku: null,
  imagesTotal: 0,
  imageCurrent: 0,
  startTime: Date.now(),
  endTime: Date.now(),
  state: "idle",
  messages: [],
  imagesCounter: 0,
};

export const states = [
  { name: "processing", title: "Обрабатывается...", class: "info" },
  { name: "error", title: "Ошибка", class: "error" },
  { name: "cancel", title: "Отменено", class: "warning" },
  { name: "finished", title: "Обработка завершена", class: "success" },
  { name: "canceling", title: "Отменяется...", class: "info" },
];

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
export const firebaseConfig = {
  apiKey: "AIzaSyCxHzxKeO7SOTHGKplhAaUwiqgFkgxZE_Y",
  authDomain: "crafta-parser.firebaseapp.com",
  databaseURL: "https://crafta-parser.firebaseio.com",
  projectId: "crafta-parser",
  storageBucket: "crafta-parser.appspot.com",
  messagingSenderId: "566745129113",
  appId: "1:566745129113:web:60b331446b0d3a91795808",
  measurementId: "G-31D5F3BBSZ"
};
