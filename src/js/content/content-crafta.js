import NProgress from "nprogress";
import "@/css/content-crafta.css";
import { mdiPlus } from "@mdi/js";
import { genIcon } from "../common/icons";
import { isNil } from "lodash";

let appState = "unauthorized";

export const buttons = [
  {
    type: "product",
    text: genIcon(mdiPlus, "#F28612"),
    class: ["ca-button-p"],
    title: [
      { state: false, text: "Добавить товар" },
      { state: true, text: "Удалить товар" },
    ],
  },
  {
    type: "product-bl",
    text: "ЧС",
    class: ["ca-button-pbl"],
    title: [
      { state: false, text: "Добавить товар в ЧС" },
      { state: true, text: "Удалить товар из ЧС" },
    ],
  },
  {
    type: "seller-bl",
    text: "ЧСП",
    class: ["ca-button-sbl"],
    title: [
      { state: false, text: "Добавить продавца в ЧС" },
      { state: true, text: "Удалить продавца из ЧС" },
    ],
  },
];

export const init = (urls = [], sellers = []) => {
  // console.log("init from content-crafta");
  NProgress.configure({ easing: "ease", speed: 500, showSpinner: false });

  chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    // console.log("onMessage: ", request);
    if (request.type !== "CRAFTA_ADPTER") return;
    const { action, data } = request;
    if (action === "message") {
      setAppState("idle");
    }
    if (action === "product") {
      refreshProductBtnsState(data);
      setAppState("idle");
    }
    if (action === "seller") {
      refreshSellerBtnsState(data);
      setAppState("idle");
    }
    if (action === "appstate") {
      setAppState(data.state);
    }
    // if (request.action === "state") {
    //   if (request.data.state === "logout") setAppState("unauthorized");
    //   if (request.data.state === "get-products") getProducts(urls);
    //   if (request.data.state === "get-sellers") getSellers(sellers);
    // }
  });

  window.addEventListener("app-state-change", (event) => {
    let state = event.detail.state;
    let btns = [...document.querySelectorAll(".ca-button")];
    if (state === "loading") {
      btns.map((b) => {
        b.disabled = true;
      });
      NProgress.start();
    } else if (state === "idle") {
      btns.map((b) => {
        b.disabled = false;
      });
      NProgress.done();
    } else {
      btns.map((b) => {
        b.disabled = true;
        b.classList.remove("active");
      });
      NProgress.done();
    }
  });

  embedExtermalResources();
};

const embedExtermalResources = () => {
  const el = document.createElement("link");
  el.setAttribute(
    "href",
    "https://fonts.googleapis.com/css?family=Roboto:300,400,500&display=swap"
  );
  el.setAttribute("rel", "stylesheet");
  const head =
    document.head ||
    document.getElementsByTagName("head")[0] ||
    document.documentElement;
  head.insertBefore(el, head.lastChild);
};

const refreshProductBtnsState = ({ product, url }) => {
  const btns = document.querySelectorAll(".ca-button");
  for (let btn of btns) {
    // let p = items.find(i => i.url === btn.dataset.url);
    if (url !== btn.dataset.url) continue;
    if (isNil(product)) {
      if (["product", "product-bl"].includes(btn.dataset.type)) {
        btn.classList.remove("active");
        btn.setAttribute(
          "title",
          buttons
            .find((b) => b.type === btn.dataset.type)
            .title.find((t) => t.state === false).text
        );
      }
      continue;
    }
    const { blacklisted = false } = product;
    // if(!p) continue
    if (btn.dataset.type === "product") {
      if (isNil(product)) {
        btn.classList.remove("active");
        btn.setAttribute(
          "title",
          buttons
            .find((b) => b.type === btn.dataset.type)
            .title.find((t) => t.state === false).text
        );
        // console.log("product isNil", product);
        continue;
      } else {
        blacklisted
          ? btn.classList.remove("active")
          : btn.classList.add("active");
        btn.setAttribute(
          "title",
          buttons
            .find((b) => b.type === "product")
            .title.find((t) => t.state === !blacklisted).text
        );
        // console.log("product not isNil", product);
        continue;
      }
    }
    if (btn.dataset.type === "product-bl") {
      if (isNil(product)) {
        btn.classList.remove("active");
        btn.setAttribute(
          "title",
          buttons
            .find((b) => b.type === btn.dataset.type)
            .title.find((t) => t.state === false).text
        );
        continue;
      } else {
        blacklisted
          ? btn.classList.add("active")
          : btn.classList.remove("active");
        btn.setAttribute(
          "title",
          buttons
            .find((b) => b.type === "product-bl")
            .title.find((t) => t.state === blacklisted).text
        );
        continue;
      }
    }
  }
};

const refreshSellerBtnsState = ({ seller, url }) => {
  // console.log("refresh seller btn: ", seller);
  const btns = document.querySelectorAll(".ca-button");
  for (let btn of btns) {
    // let p = items.find(i => i.url === btn.dataset.url);
    if (url !== btn.dataset.seller) continue;
    if (isNil(seller)) {
      // console.log("seller is Nill");
      if (btn.dataset.type === "seller-bl") {
        btn.classList.remove("active");
        btn.setAttribute(
          "title",
          buttons
            .find((b) => b.type === btn.dataset.type)
            .title.find((t) => t.state === false).text
        );
      }
      continue;
    }
    if (btn.dataset.type === "seller-bl") {
      if (isNil(seller)) {
        btn.classList.remove("active");
        btn.setAttribute(
          "title",
          buttons
            .find((b) => b.type === btn.dataset.type)
            .title.find((t) => t.state === false).text
        );
        continue;
      } else {
        btn.classList.add("active");
        btn.setAttribute(
          "title",
          buttons
            .find((b) => b.type === "seller-bl")
            .title.find((t) => t.state === true).text
        );
        continue;
      }
    }
  }
};

const setAppState = (state) => {
  window.dispatchEvent(
    new CustomEvent("app-state-change", {
      detail: { state },
    })
  );
  appState = state;
};

const handleProductBtns = (e) => {
  e.preventDefault();
  const btn = e.currentTarget;
  const url = btn.dataset.url;
  const type = btn.dataset.type;
  const seller = btn.dataset.seller;
  btn.disabled = true;
  setAppState("loading");
  let payload = { type, url };
  if (seller) payload.seller = seller;
  // console.log("btn url - ", url);
  chrome.runtime.sendMessage({
    type: "CRAFTA_ADPTER",
    action: "buttons",
    from: "cs-crafta",
    data: payload,
  });
};

export const genBtn = (i, url) => {
  const btn = document.createElement("button");
  btn.classList.add("ca-button", ...i.class);
  btn.innerHTML = i.text;
  btn.dataset.url = url;
  btn.dataset.type = i.type;
  btn.disabled = true;
  btn.setAttribute(
    "title",
    buttons.find((b) => b.type === i.type).title.find((t) => t.state === false)
      .text
  );
  btn.addEventListener("click", handleProductBtns);
  return btn;
};

export const getProducts = (urls) => {
  // console.log("getProducts: ", urls);
  chrome.runtime.sendMessage({
    type: "CRAFTA_ADPTER",
    action: "products",
    from: "cs-crafta",
    data: { urls },
  });
};

export const getSellers = (sellers) => {
  // console.log("getSellers: ", sellers);
  chrome.runtime.sendMessage({
    type: "CRAFTA_ADPTER",
    action: "sellers",
    from: "cs-crafta",
    data: { sellers },
  });
};
