import {
  FetchService,
  AppStateService,
  NotifierService,
  AwakeService,
  FileService,
  SleepService,
  ImageService,
  ConfigService,
  ScriptService,
} from "../services";
import tress from "tress";
import { CancelError, NewInstallError, DownloadError } from "../errors";
import ImageParser from "./image-parser";

export default class ImageConvertor {
  constructor() {
    this.FetchService = new FetchService();
    this.AppStateService = new AppStateService();
    this.NotifierService = new NotifierService();
    this.AwakeService = new AwakeService();
    this.FileService = new FileService();
    this.SleepService = new SleepService();
    this.ImageService = new ImageService();
    this.ConfigService = new ConfigService();
    this.ScriptService = new ScriptService();
    this.ImageParser = new ImageParser();
    this.initQueue();
    this.controller = new AbortController();
  }

  initQueue() {
    this.queue = tress(this.work.bind(this), 1);
    this.queue.drain = this.onDrain.bind(this);
    this.queue.error = this.onError.bind(this);
    this.queue.success = this.onSuccess.bind(this);
  }

  resetQueue() {
    this.queue.kill();
    this.initQueue();
  }

  onDrain() {
    if (this.AppStateService.state === "cancel") return;
    this.AppStateService.updateProcessState({
      endTime: Date.now(),
      state: "finished",
    });
    const message = this.AppStateService.statInfoFormat();
    const title = this.AppStateService.getStateItem("finished").title;
    this.NotifierService.notify(`${title}: ${message}`);
    this.AppStateService.addMessage(message);
    this.AwakeService.keepAwakeCancel();
    this.AppStateService.resetProcessState();
    this.initQueue();
  }

  onError(err) {
    const { productCurrent } = this.AppStateService.processState;
    this.AppStateService.updateProcessState({
      productCurrent: productCurrent + 1,
    });
    this.handleErrors(err);
  }

  onSuccess() {
    const { productCurrent } = this.AppStateService.processState;
    this.AppStateService.updateProcessState({
      productCurrent: productCurrent + 1,
    });
  }

  handleErrors(e) {
    this.AwakeService.keepAwakeCancel();
    if (e instanceof CancelError) {
      let message = "Обработка изображений отменена.";
      this.AppStateService.addMessage(
        this.AppStateService.statInfoFormat(),
        "info"
      );
      this.resetQueue();
      this.AppStateService.addMessage(message);
      this.NotifierService.notify(message);
      this.FileService.deleteAll();
      this.AppStateService.resetProcessState();
      console.log(e.message);
      console.error(message);
      return;
    } else if (e instanceof NewInstallError) {
      this.AppStateService.updateProcessState({
        endTime: Date.now(),
        state: "idle",
      });
      console.error(e);
    } else if (e instanceof Error) {
      const { message } = e;
      this.AppStateService.updateProcessState({
        endTime: Date.now(),
        state: "error",
      });
      this.AppStateService.addMessage(message);
      this.NotifierService.notify(`Ошибка: ${message}`);
      console.error(message);
      return;
    } else {
      this.AppStateService.updateProcessState({
        endTime: Date.now(),
        state: "error",
      });
      this.AppStateService.addMessage(e.toString());
      console.error(e);
    }
  }

  async handleCancel() {
    this.AppStateService.updateProcessState({ state: "canceling" });
    this.AwakeService.keepAwakeCancel();
    await this.ScriptService.runScript("cancel");
    this.AppStateService.updateProcessState({
      endTime: Date.now(),
      state: "cancel",
    });
    this.AppStateService.controller.abort();
  }

  work(product, done) {
    this.AppStateService.updateProcessState({
      productCurrentSku: product.sku,
    });
    this.FetchService.fetchRetry(product.productUrl)
      .then((response) => response.text())
      .then(this.ImageParser.parse)
      .then((images) => this.processImages(images, product))
      .then(() => done(null))
      .catch((e) => done(e));
  }

  async processImages(images, product) {
    this.AppStateService.updateProcessState({
      imagesTotal: images.length,
      imageCurrent: 0,
    });
    for (let [index, image] of images.entries()) {
      try {
        if (this.AppStateService.state === "cancel")
          throw new CancelError("processImages");
        await this.FileService.download(image)
          .then((d) => this.processFile(d, product, index))
          .then((d) => this.FileService.delete(d));
        this.AppStateService.updateProcessState({
          imageCurrent: index + 1,
          imagesCounter: ++this.AppStateService.processState.imagesCounter,
        });
      } catch (e) {
        if (e instanceof DownloadError) {
          console.error("Download error: ", e.message);
          this.AppStateService.addImageError(product.sku, index + 1);
          this.AppStateService.updateProcessState({
            imageCurrent: index + 1,
          });
          await this.SleepService.sleep(1000);
          continue;
        } else {
          throw e;
        }
      }
    }
    return null;
  }

  async processFile(d, product, k) {
    const prefs = await this.ConfigService.getConfig();
    const scale = await this.ImageService.imageScale(d);
    const resp = await this.ScriptService.runScript("convertor", [
      d.filename,
      scale,
      product,
      prefs,
      k + 1,
    ]);
    if (resp) {
      const err = resp.stderr || resp.error;
      console.log("Node script out: ", resp.stdout);
      if (err) {
        console.log("Node script resp.stderr: ", resp.stderr);
        console.log("Node script resp.error: ", resp.error);
        if (
          !resp.error &&
          typeof resp.stderr === "string" &&
          resp.stderr.toLowerCase().includes("warning") &&
          this.AppStateService.state !== "cancel"
        ) {
          return d;
        } else if (this.AppStateService.state === "cancel") {
          throw new CancelError("processFile");
        }
        throw new Error("Ошибка обработки изображения");
      }
      return d;
    } else {
      chrome.tabs.create({
        url: "helper.html",
      });
      throw new NewInstallError("Новая установка");
    }
  }

  processRequest(request) {
    const { products } = request;
    if (!products) return;
    if (!Array.isArray(products)) return;
    this.queue.push(products);
    const { productsTotal } = this.AppStateService.processState;
    this.AppStateService.updateProcessState({
      productsTotal: productsTotal + products.length,
    });
  }

  init() {
    chrome.runtime.onMessage.addListener((request) => {
      if (request.type !== "CRAFTA_ADPTER") return;
      if (request.from === "bs") return;
      if (request.action === "images") {
        if (this.queue.idle()) {
          this.NotifierService.notify("Начинаем обработку изображений");
        }
        if (this.AppStateService.isIdle) {
          this.AppStateService.updateProcessState({
            startTime: Date.now(),
            state: "processing",
            messages: [],
          });
          this.AppStateService.imageErrorrs = [];
        }
        this.AwakeService.keepAwake();
        this.processRequest(request.data);
        return;
      }
      if (request.from === "popup" && request.action === "cancel") {
        this.handleCancel();
        return;
      }
    });
  }
}
