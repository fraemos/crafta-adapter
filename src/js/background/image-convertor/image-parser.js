export default class ImageParser {
  constructor() {
    this.parser = new DOMParser();
    this.parse = this.parse.bind(this);
  }

  async parse(str) {
    const doc = this.getDoc(str);
    return this.getImages(doc);
  }

  getDoc(str) {
    return this.parser.parseFromString(str, "text/html");
  }

  getSsrProps(doc, title) {
    const props = doc
      .querySelector(`[data-bazooka="${title}"]`)
      .getAttribute("data-ssr-props");
    if (!props) throw new Error("No ssr props");
    return JSON.parse(props);
  }

  getImages(doc) {
    try {
      const parsed = this.getSsrProps(doc, "Product");
      return Object.values(JSON.parse(parsed.photos)).map((p) => p.original);
    } catch (e) {
      throw new Error("Невозможно распарсить изображения товара");
    }
  }
}
