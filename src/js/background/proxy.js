// Notes: we need the `sourceTabUrl &&` before the URL check because chromebooks make weird requests that don't come from "real" tabs.

let accessHeaders = new Map();
let tabIdToUrlMap = new Map();

let requestListener = function(details) {
  const accessControlRequestHeader = details.requestHeaders.find(
    elem => elem.name.toLowerCase() === "access-control-request-headers"
  );
  if (accessControlRequestHeader) {
    accessHeaders.set(details.requestId, accessControlRequestHeader.value);
  }
};

let responseListener = function(details) {
  let responseHeaders = details.responseHeaders;
  let sourceTabUrl = tabIdToUrlMap.get(details.tabId);
  let hostname = sourceTabUrl ? new URL(sourceTabUrl).hostname : undefined;
  if (["localhost", "crafta.bang-bang.su"].includes(hostname)) {
    responseHeaders = responseHeaders.filter(
      elem =>
        elem.name.toLowerCase() !== "access-control-allow-origin" &&
        elem.name.toLowerCase() !== "access-control-allow-methods"
    );
    responseHeaders.push({
      name: "Access-Control-Allow-Origin",
      value: details.initiator
    });
    responseHeaders.push({
      name: "Access-Control-Allow-Methods",
      value: "GET, PUT, POST, DELETE, HEAD, OPTIONS"
    });

    if (accessHeaders.has(details.requestId)) {
      responseHeaders.push({
        name: "Access-Control-Allow-Headers",
        value: accessHeaders.get(details.requestId)
      });
      accessHeaders.delete(details.requestId);
    }
  }
  return { responseHeaders };
};

let tabUpdateListener = function(tabId, changeInfo, tab) {
  tabIdToUrlMap.set(tabId, tab.url);
};

let tabCreatedListener = function(tab) {
  tabIdToUrlMap.set(tab.id, tab.url);
};

function reload() {
  tabIdToUrlMap = new Map();

  try {
    chrome.webRequest.onHeadersReceived.removeListener(responseListener);
    chrome.webRequest.onBeforeSendHeaders.removeListener(requestListener);
  } catch (e) {}

  chrome.webRequest.onHeadersReceived.addListener(
    responseListener,
    { urls: ["<all_urls>"] },
    ["blocking", "responseHeaders"]
  );
  chrome.webRequest.onBeforeSendHeaders.addListener(
    requestListener,
    { urls: ["<all_urls>"] },
    ["blocking", "requestHeaders"]
  );

  chrome.tabs.query({}, function(tabs) {
    for (let tab of tabs) {
      tabIdToUrlMap.set(tab.id, tab.url);
    }
  });
}

export const init = () => {
  chrome.tabs.onUpdated.addListener(tabUpdateListener);
  chrome.tabs.onCreated.addListener(tabCreatedListener);

  chrome.runtime.onInstalled.addListener(reload);
  chrome.runtime.onStartup.addListener(reload);
};
