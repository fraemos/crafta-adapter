import firebase from "@/js/common/firebase";
import { notify } from "@/js/common/notify";
import "firebase/firestore";
import { isNil } from "lodash";
import { sendMessageToCrafta } from "./router";

class MapWrapper {
  getMethods(obj) {
    let properties = new Set();
    let currentObj = obj;
    do {
      Object.getOwnPropertyNames(currentObj).map((item) =>
        properties.add(item)
      );
    } while ((currentObj = Object.getPrototypeOf(currentObj)));
    return [...properties.keys()].filter(
      (item) => typeof obj[item] === "function"
    );
  }

  /* #wrappedMap */
  constructor(wrappedMap, name) {
    this.wrappedMap = wrappedMap;
    this.name = name;
    this.methods = this.getMethods(this.wrappedMap);
    for (const methodName of this.methods) {
      MapWrapper.prototype[methodName] = function (...args) {
        console.log(`function ${methodName} call for ${this.name}`);
        const result = this.wrappedMap[methodName](...args);
        console.log(`current ${this.name}: `, this.wrappedMap);
        return result;
      };
    }
  }
}

const db = firebase.firestore();

const listenersMap = new Map();
const tabsMap = new Map();
const productsMap = new Map();
const sellersMap = new Map();

// const listenersMapRaw = new Map();
// const tabsMapRaw = new Map();
// const productsMapRaw = new Map();
// const sellersMapRaw = new Map();

// const listenersMap = new MapWrapper(listenersMapRaw, "listenersMap");
// const tabsMap = new MapWrapper(tabsMapRaw, "tabsMap");
// const productsMap = new MapWrapper(productsMapRaw, "productsMap");
// const sellersMap = new MapWrapper(sellersMapRaw, "sellersMap");

export const init = () => {
  chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    // console.log("firebase onMessage: ", request);
    const { action, data, type, from } = request;
    if (type !== "CRAFTA_ADPTER") return;
    if (from === "bs") return;
    if (from === "firebase") return;
    if (action === "user") {
      setUser(firebase.auth().currentUser);
    }
    if (from === "popup") {
      // if (action === "login") {
      //   handleLogin(data);
      // }
      if (action === "logout") {
        handleLogout();
      }
    }
    if (from === "cs-crafta") {
      if (action === "products") {
        // console.log("sender: ", sender);
        // console.log("data: ", request);
        const { urls } = data;
        if (!urls) return;
        if (tabsMap.has(sender.tab.id)) {
          const tab = tabsMap.get(sender.tab.id);
          tabsMap.set(sender.tab.id, { ...tab, urls });
        } else {
          tabsMap.set(sender.tab.id, {
            tab: sender.tab,
            urls,
          });
        }
        subscribeToProducts(urls);
      }
      if (action === "sellers") {
        // console.log("sender: ", sender);
        // console.log("data: ", data);
        const { sellers } = data;
        if (!sellers) return;
        if (tabsMap.has(sender.tab.id)) {
          const tab = tabsMap.get(sender.tab.id);
          tabsMap.set(sender.tab.id, { ...tab, sellers });
        } else {
          tabsMap.set(sender.tab.id, {
            tab: sender.tab,
            sellers,
          });
        }
        subscribeToSellers(sellers);
      }
      if (action === "buttons") {
        handleButtons(data);
      }
    }

    return true;
  });
  firebase.auth().onAuthStateChanged((user) => {
    if (user) {
      // // User is signed in.
      // var displayName = user.displayName;
      // var email = user.email;
      // var emailVerified = user.emailVerified;
      // var photoURL = user.photoURL;
      // var isAnonymous = user.isAnonymous;
      // var uid = user.uid;
      // var providerData = user.providerData;
      setUser(user);
      subscribeToAll();
      // notify("User is signed in.");
      // console.log("User is signed in.", user);
      // console.log("firebase: ", firebase.auth().currentUser);
      // ...
    } else {
      // User is signed out.
      setUser(null);
      unsubscribeFromAll();
      sendMessageToCrafta({
        type: "CRAFTA_ADPTER",
        action: "appstate",
        from: "firebase",
        data: {
          state: "unauthorized",
        },
      });
      // notify("User is signed out.");
      // console.log("User is signed out.");
    }
  });
  chrome.tabs.onRemoved.addListener((tabId, removeInfo) => {
    // console.log("tab onRemoved: ", tabId);
    if (!tabsMap.has(tabId)) return;
    const tab = tabsMap.get(tabId);
    const { urls = [], sellers = [] } = tab;
    tabsMap.delete(tabId);
    urls.forEach((url) => {
      let hasUrl = false;
      if (tabsMap.size) {
        for (let [tabId, tab] of tabsMap) {
          if (!tab.urls) continue;
          if (tab.urls.includes(url)) hasUrl = true;
        }
      }
      if (!hasUrl) {
        if (!listenersMap.has(url)) return;
        const listener = listenersMap.get(url);
        listener.unsubscribe();
        listenersMap.delete(url);
        productsMap.delete(url);
      }
    });
    sellers.forEach((url) => {
      let hasUrl = false;
      if (tabsMap.size) {
        for (let [tabId, tab] of tabsMap) {
          if (!tab.sellers) continue;
          if (tab.sellers.includes(url)) hasUrl = true;
        }
      }
      if (!hasUrl) {
        if (!listenersMap.has(url)) return;
        const listener = listenersMap.get(url);
        listener.unsubscribe();
        listenersMap.delete(url);
        sellersMap.delete(url);
      }
    });
  });
  chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    if (!tabsMap.has(tabId)) return;
    const { url } = changeInfo;
    if (isNil(url)) return;
    if (url.match(/crafta\.ua\/(catalog|products)/g)) return;
    const tabItem = tabsMap.get(tabId);
    const { urls = [], sellers = [] } = tabItem;
    tabsMap.delete(tabId);
    urls.forEach((url) => {
      let hasUrl = false;
      if (tabsMap.size) {
        for (let [tabId, tab] of tabsMap) {
          if (!tab.urls) continue;
          if (tab.urls.includes(url)) hasUrl = true;
        }
      }
      if (!hasUrl) {
        if (!listenersMap.has(url)) return;
        const listener = listenersMap.get(url);
        listener.unsubscribe();
        listenersMap.delete(url);
        productsMap.delete(url);
      }
    });
    sellers.forEach((url) => {
      let hasUrl = false;
      if (tabsMap.size) {
        for (let [tabId, tab] of tabsMap) {
          if (!tab.sellers) continue;
          if (tab.sellers.includes(url)) hasUrl = true;
        }
      }
      if (!hasUrl) {
        if (!listenersMap.has(url)) return;
        const listener = listenersMap.get(url);
        listener.unsubscribe();
        listenersMap.delete(url);
        sellersMap.delete(url);
      }
    });
  });
  chrome.tabs.onActivated.addListener((activeInfo) => {
    const { tabId } = activeInfo;
    if (tabsMap.has(tabId)) {
      if (!firebase.auth().currentUser) {
        sendMessageToCrafta({
          type: "CRAFTA_ADPTER",
          action: "appstate",
          from: "firebase",
          data: {
            state: "unauthorized",
          },
        });
        return;
      }
      const { urls = [], sellers = [] } = tabsMap.get(tabId);
      if (urls.length) subscribeToProducts(urls);
      if (sellers.length) subscribeToSellers(sellers);
    }
  });
};

const subscribeToAll = () => {
  const urlsSet = new Set();
  const sellersSet = new Set();
  for (let { urls, sellers } of tabsMap.values()) {
    if (urls) urls.forEach((url) => urlsSet.add(url));
    if (sellers) sellers.forEach((url) => sellersSet.add(url));
  }
  if (urlsSet.size) subscribeToProducts([...urlsSet]);
  if (sellersSet.size) subscribeToSellers([...sellersSet]);
};

const unsubscribeFromAll = () => {
  const urlsSet = new Set();
  const sellersSet = new Set();
  for (let { urls, sellers } of tabsMap.values()) {
    if (urls) urls.forEach((url) => urlsSet.add(url));
    if (sellers) sellers.forEach((url) => sellersSet.add(url));
  }
  for (let url of urlsSet.values()) {
    if (!listenersMap.has(url)) continue;
    const listener = listenersMap.get(url);
    listener.unsubscribe();
    listenersMap.delete(url);
    productsMap.delete(url);
  }
  for (let url of sellersSet.values()) {
    if (!listenersMap.has(url)) continue;
    const listener = listenersMap.get(url);
    listener.unsubscribe();
    listenersMap.delete(url);
    sellersMap.delete(url);
  }
};

const subscribeToProducts = (urls) => {
  const user = firebase.auth().currentUser;
  if (!user) return;
  const userId = user.uid;
  const productsRef = db.collection(`users/${userId}/products`);
  urls.forEach(async (url) => {
    if (productsMap.has(url)) {
      const { doc } = productsMap.get(url);
      // console.log("product exist: ", doc.data());
      sendMessageToCrafta({
        type: "CRAFTA_ADPTER",
        action: "product",
        from: "firebase",
        data: {
          product: doc.data() || null,
          url,
        },
      });
      return;
    }
    if (listenersMap.has(url)) return;
    const snap = await productsRef.where("productUrl", "==", url).get();
    // if(!snap.empty)
    // console.log("snap: ", snap);
    const docRef = snap.empty ? productsRef.doc() : snap.docs[0].ref;
    // console.log("docRef", docRef);
    const unsubscribe = docRef.onSnapshot(function (doc) {
      if (!listenersMap.has(url)) {
        listenersMap.set(url, {
          url,
          unsubscribe,
        });
      }
      productsMap.set(url, {
        url,
        doc,
      });
      // console.log("product new: ", doc.data());
      sendMessageToCrafta({
        type: "CRAFTA_ADPTER",
        action: "product",
        from: "firebase",
        data: {
          product: doc.data() || null,
          url,
        },
      });
    });
  });
};

const subscribeToSellers = (sellers) => {
  const user = firebase.auth().currentUser;
  if (!user) return;
  const userId = user.uid;
  const sellersRef = db.collection(`users/${userId}/sellers`);
  sellers.forEach(async (url) => {
    if (sellersMap.has(url)) {
      const { doc } = sellersMap.get(url);
      // console.log("product exist: ", doc.data());
      sendMessageToCrafta({
        type: "CRAFTA_ADPTER",
        action: "seller",
        from: "firebase",
        data: {
          seller: doc.data() || null,
          url,
        },
      });
      return;
    }
    if (listenersMap.has(url)) return;
    const snap = await sellersRef
      .where("url", "==", url)
      .where("blacklisted", "==", true)
      .get();
    // if(!snap.empty)
    // console.log("snap: ", snap);
    const docRef = snap.empty ? sellersRef.doc() : snap.docs[0].ref;
    // console.log("docRef", docRef);
    const unsubscribe = docRef.onSnapshot(function (doc) {
      if (!listenersMap.has(url)) {
        listenersMap.set(url, {
          url,
          unsubscribe,
        });
      }
      sellersMap.set(url, {
        url,
        doc,
      });
      // console.log("product new: ", doc.data());
      sendMessageToCrafta({
        type: "CRAFTA_ADPTER",
        action: "seller",
        from: "firebase",
        data: {
          seller: doc.data() || null,
          url,
        },
      });
    });
  });
};

const handleButtons = (data) => {
  // console.log("button: ", data);
  const { type, url, seller: sellerUrl } = data;
  if (type === "product") {
    if (!productsMap.has(url)) return;
    const product = productsMap.get(url);
    // console.log("handleButtons product: ", product);
    const { doc: docSnap } = product;
    if (docSnap.exists) {
      const { blacklisted } = docSnap.data();
      if (blacklisted) {
        // console.log("product update");
        const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp();
        return docSnap.ref.update({ blacklisted: false, updateTimestamp: serverTimestamp });
      } else {
        // console.log("product delete");
        // products.delete(url);
        return docSnap.ref.delete();
      }
    } else {
      // console.log("add product");
      const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp();
      const newProductData = {
        productUrl: url,
        note: "",
        shop: [],
        blacklisted: false,
        productStatus: "new",
        author: firebase.auth().currentUser.uid,
        sellerData: null,
        stopWords: [],
        stopWordsStatus: "new",
        crawlerItemDisable: true,
        shopStatus: "absent",
        createTimestamp: serverTimestamp,
        updateTimestamp: serverTimestamp,
        cid: null,
      };
      return docSnap.ref.set(newProductData);
    }
  }
  if (type === "product-bl") {
    if (!productsMap.has(url)) return;
    const product = productsMap.get(url);
    // console.log("handleButtons product: ", product);
    const { doc: docSnap } = product;
    if (docSnap.exists) {
      const { blacklisted } = docSnap.data();
      if (blacklisted) {
        // products.delete(url);
        return docSnap.ref.delete();
      } else {
        const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp();
        return docSnap.ref.update({ blacklisted: true, updateTimestamp: serverTimestamp });
      }
    } else {
      // console.log("add product-bl");
      const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp();
      const newProductData = {
        productUrl: url,
        note: "",
        shop: [],
        blacklisted: true,
        productStatus: "new",
        author: firebase.auth().currentUser.uid,
        sellerData: null,
        stopWords: [],
        stopWordsStatus: "nodata",
        crawlerItemDisable: true,
        shopStatus: "absent",
        createTimestamp: serverTimestamp,
        updateTimestamp: serverTimestamp,
        cid: null,
      };
      return docSnap.ref.set(newProductData);
    }
  }
  if (type === "seller-bl") {
    if (!sellerUrl) {
      notify(
        "Не удалось распознать продавца. Добавить продавца в ЧС можно только на странице товара."
      );
      sendMessageToCrafta({
        type: "CRAFTA_ADPTER",
        action: "appstate",
        from: "firebase",
        data: {
          state: "idle",
        },
      });
      return;
    }
    if (!sellersMap.has(sellerUrl)) return;
    const seller = sellersMap.get(sellerUrl);
    // console.log("handleButtons product: ", product);
    const { doc: docSnap } = seller;
    if (docSnap.exists) {
      const { blacklisted } = docSnap.data();
      if (blacklisted) {
        // products.delete(url);
        return docSnap.ref.delete();
      } else {
        const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp();
        return docSnap.ref.update({ blacklisted: true, updateTimestamp: serverTimestamp });
      }
    } else {
      // console.log("add product-bl");
      const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp();
      const newData = {
        url: sellerUrl,
        nickname: "",
        phone: "",
        email: "",
        note: "",
        productsCount: 0,
        blacklisted: true,
        author: firebase.auth().currentUser.uid,
        createTimestamp: serverTimestamp,
        updateTimestamp: serverTimestamp,
      };
      return docSnap.ref.set(newData);
    }
  }
};

const handleLogin = (data) => {
  const { email, password } = data;
  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then(() => {
      notify("Login success!");
    })
    .catch((error) => {
      // Handle Errors here.
      const { code: errorCode, message: errorMessage } = error;
      // [START_EXCLUDE]
      if (errorCode === "auth/wrong-password") {
        notify("Неверный пароль.");
      } else {
        notify(errorMessage);
      }
      console.log(error);
      // [END_EXCLUDE]
    });
};

const handleLogout = () => {
  firebase
    .auth()
    .signOut()
    .then(function () {
      // Sign-out successful.
      // notify("Sign-out successful.");
    })
    .catch(function (error) {
      // An error happened.
      notify("An error happened during logout.");
    });
};

const setUser = (user) => {
  chrome.runtime.sendMessage({
    type: "popup",
    action: "user",
    data: user,
  });
  // console.log("setUser", user);
  // chrome.storage.local.set({ user });
};
