export default class ScriptService {
  constructor() {
    this.host = "com.add0n.node";
    this.scripts = {
      cancel: {
        path: "./node/cancel.js",
        permissions: ["child_process", "os"],
      },
      keepAwake: {
        path: "./node/keep-awake.js",
        permissions: ["child_process", "os"],
      },
      keepAwakeCancel: {
        path: "./node/keep-awake-cancel.js",
        permissions: ["child_process", "os"],
      },
      convertor: {
        path: "./node/index.js",
        permissions: ["child_process", "os", "path", "fs"],
      },
    };
  }

  getScript(name) {
    try {
      return fetch(this.scripts[name].path).then((response) => response.text());
    } catch (e) {
      throw new Error("Script not found");
    }
  }

  async runScript(name, args) {
    const script = await this.getScript(name);
    return new Promise((resolve) => {
      chrome.runtime.sendNativeMessage(
        this.host,
        {
          permissions: this.scripts[name].permissions,
          script,
          args,
        },
        (response) => resolve(response)
      );
    });
  }
}
