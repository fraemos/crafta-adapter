import { CancelError } from "../errors";
import AppStateService from "./app-state";

export default class SleepService {
  constructor() {
    this.AppStateService = new AppStateService();
  }

  sleep(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  cancelableSleep({ signal }, ms) {
    return new Promise((resolve, reject) => {
      // Something fake async
      const timeout = window.setTimeout(resolve, ms);
      // Listen for abort event on signal
      signal.addEventListener(
        "abort",
        () => {
          window.clearTimeout(timeout);
          this.AppStateService.controller = new AbortController();
          reject(new CancelError("Cancel"));
        },
        { once: true }
      );
    });
  }
}
