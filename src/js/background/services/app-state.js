export default class AppStateService {
  constructor() {
    if (AppStateService.exists) {
      return AppStateService.instance;
    }
    AppStateService.instance = this;
    AppStateService.exists = true;
    this.processState = {
      productsTotal: 0,
      productCurrent: 0,
      productCurrentSku: null,
      imagesTotal: 0,
      imageCurrent: 0,
      startTime: Date.now(),
      endTime: Date.now(),
      state: "idle",
      messages: [],
      imagesCounter: 0,
    };
    this.imageErrorrs = [];
    this._states = [
      { name: "processing", title: "Обрабатывается...", class: "info" },
      { name: "error", title: "Ошибка", class: "error" },
      { name: "cancel", title: "Отменено", class: "warning" },
      { name: "finished", title: "Обработка завершена", class: "success" },
      { name: "canceling", title: "Отменяется...", class: "info" },
      { name: "idle", title: "Crafta parser", class: "info" },
    ];
    this.controller = new AbortController();
  }

  getStateItem(name) {
    return this._states.find((s) => s.name === name);
  }

  get state() {
    return this.processState.state;
  }

  get isIdle() {
    return !["processing", "canceling"].includes(this.state);
  }

  updateProcessState(data) {
    this.processState = { ...this.processState, ...data };
    this._updatePopup();
    this._updateLocalStorage();
    this._updateBadge();
  }

  resetProcessState() {
    this.updateProcessState({
      productsTotal: 0,
      productCurrent: 0,
      productCurrentSku: null,
      imagesTotal: 0,
      imageCurrent: 0,
      startTime: Date.now(),
      endTime: Date.now(),
      imagesCounter: 0,
    });
  }

  _updateBadge() {
    const { productsTotal, productCurrent } = this.processState;
    const value = productsTotal - productCurrent;
    chrome.browserAction.setBadgeText({
      text: value ? value.toString() : "",
    });
  }

  _updatePopup() {
    chrome.runtime.sendMessage({
      type: "popup",
      action: "data",
      data: this.processState,
    });
  }

  _updateLocalStorage() {
    chrome.storage.local.set({ processState: { ...this.processState } });
  }

  addMessage(message) {
    const { messages, state } = this.processState;
    if (typeof message === "string") {
      const item = this.getStateItem(state);
      messages.unshift({
        message,
        type: item ? item.class : "info",
      });
    } else messages.unshift(message);
    this.updateProcessState({ messages });
  }

  removeMessage(id) {
    let { messages } = this.processState;
    messages = messages.filter((m) => m.id !== id);
    this.updateProcessState({ messages });
  }

  addImageError(sku, k) {
    const i = this.imageErrorrs.length
      ? this.imageErrorrs.indexOf((e) => e.sku === sku)
      : null;
    i
      ? this.imageErrorrs[i].k.push(k)
      : this.imageErrorrs.push({ sku, k: [k] });
    this.removeMessage("ImageError");
    this.addMessage({
      message: `Ошибки обработки изображений: ${this.errorImageFormat(
        this.imageErrorrs
      )}`,
      type: "warning",
      id: "ImageError",
    });
  }

  errorImageFormat(errors) {
    return errors.map((e) => `${e.sku} (${e.k.join(", ")})`).join(", ");
  }

  timeFormat(time) {
    const date = new Date(time);
    const h = date.getUTCHours();
    const m = date.getUTCMinutes();
    const s = date.getSeconds();
    return `${h} ч. ${m} мин. ${s} сек.`;
  }

  statInfoFormat() {
    const {
      productCurrent,
      imagesCounter,
      endTime,
      startTime,
    } = this.processState;
    const timePassed = this.timeFormat(endTime - startTime);
    return `Обработано ${productCurrent} товаров (${imagesCounter} изображений) за ${timePassed}`;
  }
}
