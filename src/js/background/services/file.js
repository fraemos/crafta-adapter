import { DownloadError } from "../errors";

export default class FileService {
  constructor() {
    if (FileService.exists) {
      return FileService.instance;
    }
    FileService.instance = this;
    FileService.exists = true;
    this.files = new Set();
    this.downloadTimeout = 5000;
  }

  download(url) {
    const e = new DownloadError("Невозможно скачать файл изображения");
    let timerId;
    return new Promise((resolve, reject) => {
      chrome.downloads.download({ url }, (id) => {
        const observe = (d) => {
          // console.log("observe", d);
          if (d.id !== id) return;
          if (!timerId) {
            timerId = setTimeout(() => {
              chrome.downloads.cancel(id, () => {
                return reject(e);
              });
            }, this.downloadTimeout);
          }
          if (!d.state) return;
          if (!["complete", "interrupted"].includes(d.state.current)) return;
          chrome.downloads.onChanged.removeListener(observe);
          if (d.state.current !== "complete") return reject(e);
          chrome.downloads.search({ id }, ([d]) => {
            clearTimeout(timerId);
            if (!d) return reject(e);
            this.files.add(d);
            return resolve(d);
          });
        };
        chrome.downloads.onChanged.addListener(observe.bind(this));
      });
    });
  }

  delete(d) {
    return new Promise((resolve, reject) => {
      try {
        chrome.downloads.removeFile(d.id, () => {
          this.files.delete(d);
          resolve();
        });
      } catch (e) {
        console.log("Remove file error: ", e);
        reject(new Error("Ошибка во вермя удаления файла"));
      }
    });
  }

  deleteAll() {
    return Promise.all([...this.files].map((d) => this.delete(d)));
  }
}
