export default class NotifierService {
  constructor() {
    this.appTitle = chrome.runtime.getManifest().name;
  }

  notify(message) {
    chrome.notifications.create({
      title: this.appTitle,
      type: "basic",
      iconUrl: "icon-48.png",
      message,
    });
  }
}
