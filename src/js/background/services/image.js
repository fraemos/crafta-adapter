import AppStateService from "./app-state";
import FileService from "./file";
import SleepService from "./sleep";

export default class ImageService {
  constructor() {
    this.AppStateService = new AppStateService();
    this.FileService = new FileService();
    this.SleepService = new SleepService();
    this.maxScale = 4;
    this.minWidth = 1500;
    this.minHeight = 1300;
  }

  asyncImageLoader(src) {
    return new Promise((resolve, reject) => {
      const image = new Image();
      image.src = src;
      image.onload = () => resolve(image);
      image.onerror = () =>
        reject(new Error("Невозможно загрузить изображение"));
    });
  }

  async realImgDimension(src) {
    const {
      width: naturalWidth,
      height: naturalHeight,
    } = await this.asyncImageLoader(src);
    return {
      naturalWidth,
      naturalHeight,
    };
  }

  async imageScale(d) {
    let scale = 1;
    const realSize = await this.realImgDimension(d.url);
    if (
      realSize.naturalWidth < this.minWidth ||
      realSize.naturalHeight < this.minHeight
    ) {
      scale = this.minWidth / realSize.naturalWidth;
      if (realSize.naturalHeight * scale < this.minHeight) {
        scale = this.minHeight / realSize.naturalHeight;
      }
    }
    return scale > this.maxScale ? this.maxScale : scale;
  }
}
