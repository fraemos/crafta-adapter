import { CancelError, TimeoutError } from "../errors";
import AppStateService from "./app-state";
import SleepService from "./sleep";

export default class FetchService {
  constructor() {
    this.AppStateService = new AppStateService();
    this.SleepService = new SleepService();
    this.retryTimeout = 2000;
    this.requestTimeout = 5000;
    this.retryTimes = 5;
    this.userAgent = `Crafta adapter extension/${
      chrome.runtime.getManifest().version
    }`;
    this.defaultOptions = {
      headers: { "User-Agent": this.userAgent },
    };
  }

  async fetchRetry(url, options, n = this.retryTimes) {
    for (let i = 0; i < n; i++) {
      try {
        return await this.fetchWithTimeout(url, {
          ...this.defaultOptions,
          ...options,
        });
      } catch (err) {
        if (err instanceof CancelError) throw err;
        const isLastAttempt = i + 1 === n;
        if (isLastAttempt) throw err;
        await this.SleepService.cancelableSleep(
          { signal: this.AppStateService.controller.signal },
          this.retryTimeout
        );
      }
    }
  }

  async fetchWithTimeout(resource, options) {
    const { timeout = this.requestTimeout } = options;
    const controller = new AbortController();
    let reason, response;

    const listener = () => {
      reason = "cancel";
      controller.abort();
    };

    this.AppStateService.controller.signal.addEventListener("abort", listener);

    const id = setTimeout(() => {
      reason = "timeout";
      controller.abort();
    }, timeout);

    this.AppStateService.removeMessage("TimeoutError");

    try {
      response = await fetch(resource, {
        ...options,
        signal: controller.signal,
      });
      if (!response.ok) {
        throw new Error("Ошибка сети");
      }
      return response;
    } catch (e) {
      if (e instanceof AbortError) {
        if (reason === "cancel") return;
        switch (reason) {
          case "cancel":
            throw new CancelError("fetchWithTimeout");
          case "timeout":
            throw new TimeoutError("Превышение времени запроса");
          default:
            throw e;
        }
      }
      throw e;
    } finally {
      clearTimeout(id);
      this.AppStateService.controller.signal.removeEventListener(
        "abort",
        listener
      );
    }
  }
}
