export default class ConfigService {
  constructor() {
    this.defaltConfig = {
      noiseLevel: 3,
      imageQuality: 100,
      forceOpenCL: false,
      disableGpu: false,
      outputDir: "",
    };
  }

  getConfig() {
    return new Promise((resolve) => {
      chrome.storage.local.get(
        {
          ...this.defaltConfig,
        },
        (prefs) => resolve(prefs)
      );
    });
  }
}
