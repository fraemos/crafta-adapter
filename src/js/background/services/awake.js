import ScriptService from "./script";

export default class AwakeService {
  constructor() {
    this.ScriptService = new ScriptService();
  }

  keepAwake() {
    this.ScriptService.runScript("keepAwake");
  }

  keepAwakeCancel() {
    this.ScriptService.runScript("keepAwakeCancel");
  }
}
