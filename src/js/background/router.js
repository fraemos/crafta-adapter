import { notify } from "@/js/common/notify";

const getAppTab = () =>
  new Promise((resolve, reject) => {
    chrome.tabs.query(
      { url: ["*://localhost:*/*", "*://crafta.bang-bang.su/*"] },
      (tabs) =>
        tabs.length
          ? resolve(tabs[0])
          : reject(new Error("Нет открытых вкладок с приложением"))
    );
  });

const getCraftaTabs = () =>
  new Promise((resolve, reject) => {
    chrome.tabs.query({ url: ["https://*.crafta.ua/*"] }, (tabs) =>
      tabs.length
        ? resolve(tabs)
        : reject(new Error("Нет открытых вкладок с crafta.ua"))
    );
  });

const tabSendMessage = (tab, request) => {
  const tabs = Array.isArray(tab) ? tab : [tab];
  // console.log("tabSendMessage: ", tabs);
  for (let tab of tabs) {
    chrome.tabs.sendMessage(tab.id, { ...request, from: "bs" });
  }
};

export const sendMessageToCrafta = (request) => {
  // console.log("sendMessageToCrafta", request);
  getCraftaTabs()
    .then((tabs) => tabSendMessage(tabs, request))
    .catch(console.warn);
};

export const init = () => {
  chrome.runtime.onMessage.addListener((request) => {
    if (request.type !== "CRAFTA_ADPTER") return;
    if (request.from === "bs") return;
    // console.log("router onMessage: ", request);
    if (request.action === "message") {
      notify(request.data.message.message || "Пустое сообщение");
      // console.log("message", request.data);
    }
    // if (request.from === "cs-crafta") {
    //   getAppTab()
    //     .then(tab => tabSendMessage(tab, request))
    //     .catch(console.warn);
    // }
    if (request.from === "cs-app") {
      getCraftaTabs()
        .then((tabs) => tabSendMessage(tabs, request))
        .catch(console.warn);
    }
    // if (request.from === "firebase") {
    //   getCraftaTabs()
    //     .then(tabs => tabSendMessage(tabs, request))
    //     .catch(console.warn);
    // }
  });
  // chrome.tabs.onRemoved.addListener(() => {
  //   getAppTab().catch(() => {
  //     getCraftaTabs()
  //       .then(tabs =>
  //         tabSendMessage(tabs, {
  //           type: "CRAFTA_ADPTER",
  //           action: "state",
  //           data: { state: "logout" }
  //         })
  //       )
  //       .catch(() => {});
  //   });
  // });
};
