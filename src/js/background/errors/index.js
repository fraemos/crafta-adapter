import CancelError from "./cancel-error";
import DownloadError from "./download-error";
import NewInstallError from "./new-install-error";
import RetryTimeoutError from "./retry-timeout-error";
import TimeoutError from "./timeout-error";

export {
  CancelError,
  DownloadError,
  NewInstallError,
  RetryTimeoutError,
  TimeoutError,
};
