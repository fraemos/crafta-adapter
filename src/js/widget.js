import { firebaseConfig } from "./common/config";

import firebase from "firebase/app";
import "firebase/auth";
import * as firebaseui from "firebaseui";

firebase.initializeApp(firebaseConfig);

// FirebaseUI config.
const uiConfig = {
  callbacks: {
    signInSuccessWithAuthResult: function (authResult, redirectUrl) {
      window.close();
      return false;
    },
  },
  signInOptions: [
    firebase.auth.EmailAuthProvider.PROVIDER_ID,
    // firebase.auth.GoogleAuthProvider.PROVIDER_ID,
  ],
};

// Initialize the FirebaseUI Widget using Firebase.
var ui = new firebaseui.auth.AuthUI(firebase.auth());
// The start method will wait until the DOM is loaded to include the FirebaseUI sign-in widget
// within the element corresponding to the selector specified.
ui.start("#firebaseui-auth-container", uiConfig);
