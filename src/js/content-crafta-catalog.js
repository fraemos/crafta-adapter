import "../css/content-crafta-catalog.css";
import { genBtn, init, buttons, getProducts } from "./content/content-crafta";
import { mdiCheckCircle, mdiBlockHelper } from "@mdi/js";
import { genIcon } from "./common/icons";
import { isNil } from "lodash";

const imageMasks = [
  {
    id: "inList",
    icon: mdiCheckCircle,
    text: "Товар добавлен",
    class: "ca-image-mask__in-list",
  },
  {
    id: "inPBl",
    icon: mdiBlockHelper,
    text: "Товар в ЧС",
    class: "ca-image-mask__in-pbl",
  },
];

const genImageMask = (type) => {
  let mask = document.createElement("div");
  let m = imageMasks.find((m) => m.id === type);
  if (!m) return;
  mask.classList.add("ca-image-mask", m.class);
  let icon = document.createElement("div");
  icon.classList.add("ca-image-mask__icon");
  icon.innerHTML = genIcon(m.icon, "#ffffff");
  mask.appendChild(icon);
  let text = document.createElement("div");
  text.classList.add("ca-image-mask__text");
  text.innerHTML = m.text;
  mask.appendChild(text);
  return mask;
};

const updateImageMask = (data) => {
  const { product, url } = data;
  const productsList = getProductItems();
  for (let i of productsList) {
    const imageHolder = i.querySelector(".cft-product-item__image-holder");
    const itemUrl = getItemUrl(i);
    if (itemUrl !== url) continue;
    const mask = imageHolder.querySelector(".ca-image-mask");
    if (mask) mask.remove();
    if (isNil(product)) continue;
    const { blacklisted = false } = product;
    if (!blacklisted) {
      imageHolder.appendChild(genImageMask("inList"));
      continue;
    } else {
      imageHolder.appendChild(genImageMask("inPBl"));
      continue;
    }
  }
};

const getProductItems = () => {
  const productsList = document.querySelectorAll(".cft-product-item");
  if (!productsList.length) return [];
  const itemsArr = Array.from(productsList);
  return itemsArr.filter((i) => i.querySelector(".cft-product-item__price"));
};

const getItemUrl = (i) => {
  let url = i.querySelector(".cft-product-item__title").getAttribute("href");
  if (!url) {
    url = i.querySelector(".cft-product-item__link").getAttribute("href");
  }
  return url;
};

const initCatalogPage = () => {
  // console.log("initCatalogPage");
  const urls = [];
  const productsList = getProductItems();
  for (let i of productsList) {
    const url = getItemUrl(i);
    // console.log("url", url);
    if (!url) continue;
    const imageHolder = i.querySelector(".cft-product-item__image-holder");
    urls.push(url);
    // console.log(url)
    let btnsWrap = document.createElement("div");
    btnsWrap.classList.add("ca-buttons-wrap");
    buttons.forEach((b) => btnsWrap.appendChild(genBtn(b, url)));
    let imageHolderWrapper = document.createElement("div");
    imageHolderWrapper.classList.add("ca-image-holder-wrapper");
    imageHolder.parentNode.insertBefore(imageHolderWrapper, imageHolder);
    imageHolderWrapper.appendChild(imageHolder);
    imageHolderWrapper.appendChild(btnsWrap);
  }
  if (urls.length) getProducts(urls);
  init(urls);
};

chrome.runtime.onMessage.addListener((request) => {
  const { action, data, type } = request;
  if (type !== "CRAFTA_ADPTER") return;
  if (action === "product") {
    updateImageMask(data);
  }
});

window.addEventListener("app-state-change", (event) => {
  let state = event.detail.state;
  if (!["idle", "loading"].includes(state)) {
    let masks = [...document.querySelectorAll(".ca-image-mask")];
    if (masks.length) masks.map((m) => m.remove());
  }
});

initCatalogPage();
