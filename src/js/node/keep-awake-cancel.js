const os = require("os").platform()
const {exec} = require("child_process")
const cmd = `ps aux | grep -v grep | grep caffeinate | awk '{print $2}' | xargs kill -9 $1`
if(os === 'darwin') {
  exec(cmd, (error, stdout, stderr) => {
    push({ error, stdout, stderr })
    done()
  })
} else done()
