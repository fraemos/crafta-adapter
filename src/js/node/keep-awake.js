const os = require("os").platform()
const {exec} = require("child_process")
const cmd = `nohup caffeinate &>/dev/null &`
if(os === 'darwin') {
  exec(cmd, (error, stdout, stderr) => {
    push({ error, stdout, stderr })
    done()
  })
} else done()
