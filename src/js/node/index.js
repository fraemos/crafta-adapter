const [input, scale, product, prefs, k] = args
const path = require("path")
const fs = require("fs")
const os = require("os").platform()
const pDir =
  prefs.outputDir !== ""
    ? path.normalize(prefs.outputDir)
    : path.dirname(input)
const hostODir = path.join(pDir, product.sku)
!fs.existsSync(hostODir) && fs.mkdir(hostODir, {recursive: true}, () => {})
const o = path.join(pDir, product.sku, k.toString())
const i = input
let cmd = os === 'darwin' ? "/usr/local/bin/waifu2x-converter-cpp" : "waifu2x-converter-cpp"
cmd += prefs.forceOpenCL ? " --force-OpenCL" : ""
cmd += prefs.disableGpu ? " --disable-gpu" : ""
cmd += ` --scale-ratio ${scale}`
cmd += ` --noise-level ${prefs.noiseLevel}`
cmd += ` -i "${i}"`
cmd += ` -o "${o}"`
cmd += ' -f jpeg'
cmd += ` -q ${prefs.imageQuality}`
require("child_process").exec(cmd, (error, stdout, stderr) => {
  push({ error, stdout, stderr })
  done()
});
