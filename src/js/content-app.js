window.addEventListener("message", function(event) {
  // We only accept messages from ourselves
  if (event.source != window) return
  if(event.data.type !== 'CRAFTA_ADPTER') return
  if(event.data.from !== 'app') return

  chrome.runtime.sendMessage({...event.data, from: 'cs-app'})
}, false)

chrome.runtime.onMessage.addListener(request => {
  if(request.from === 'cs-app') return
  window.postMessage({...request, from: 'cs-app' }, "*")
  return true
})

const init = () => {
  const script = document.createElement('meta')
  script.setAttribute("name", "crafta-adapter-id")
  script.setAttribute("content", chrome.runtime.id)
  const head = document.head || document.getElementsByTagName("head")[0] || document.documentElement
  head.insertBefore(script, head.lastChild)
}

init()