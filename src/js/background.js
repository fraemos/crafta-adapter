import "../img/icon-16.png";
import "../img/icon-32.png";
import "../img/icon-48.png";
import "../img/icon-128.png";
import "../img/error.svg";
import "../img/info.svg";
import "../img/success.svg";

// import { init as proxyInit } from "./background/proxy";
import ImageConvertor from "./background/image-convertor";
import { init as routerInit } from "./background/router";
import { init as firebaseInit } from "./background/firebase";

// proxyInit();
const imageConvertor = new ImageConvertor();
imageConvertor.init();
// imageConverterInit();
routerInit();
firebaseInit();
