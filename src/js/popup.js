import "../css/popup.css";
import { processStateDefaults, states } from "./common/config";

let timerId = null;
let currentData;

chrome.runtime.onMessage.addListener((request) => {
  const { type, action, data } = request;
  if (type !== "popup") return null;
  if (action === "data") {
    setData(data);
  }
  if (action === "user") {
    setUser(data);
  }
});

const setData = (data) => {
  const pc = (data.productCurrent * 100) / data.productsTotal;
  document
    .getElementById("products-progress-bar")
    .setAttribute("style", `--p:${pc}%`);
  document.getElementById(
    "products-progress-bar__text"
  ).innerHTML = `${data.productCurrent}/${data.productsTotal}`;
  const ic = (data.imageCurrent * 100) / data.imagesTotal;
  document
    .getElementById("images-progress-bar")
    .setAttribute("style", `--p:${ic}%`);
  document.getElementById(
    "images-progress-bar__text"
  ).innerHTML = `${data.imageCurrent}/${data.imagesTotal}`;
  document.getElementById(
    "images-progress-bar__label"
  ).innerHTML = `Изображения товара ${data.productCurrentSku}:`;
  const progressInfo = document.getElementById("process-info");
  const cancel = document.getElementById("cancel");
  const loader = document.getElementById("loader");
  const stateInfo = states.find((s) => s.name === data.state);
  document.getElementById("state-title").innerHTML = stateInfo
    ? stateInfo.title
    : "Crafta adapter";
  if (["processing", "canceling"].includes(data.state)) {
    if (!timerId) clockStart(data.startTime);
    // removeAllMessages()
  } else {
    if (timerId) clockStop();
    else setTime(data.endTime - data.startTime);
  }
  setMessage(data);
  showHideHandler(data, progressInfo, ["processing", "canceling"]);
  showHideHandler(data, cancel, ["processing"]);
  showHideHandler(data, loader, ["canceling"]);
  currentData = data;
  // console.log("setData: ", data);
};

const setUser = (user) => {
  const loginBtn = document.getElementById("auth-section__login");
  const logoutBtn = document.getElementById("auth-section__logout");
  const userInfo = document.getElementById("auth-section__user-info");
  const avatar = document.getElementById("user-info__avatar");
  const email = document.getElementById("user-info__email");
  if (user) {
    show(logoutBtn);
    hide(loginBtn);
    show(userInfo);
    email.innerHTML = user.email;
    avatar.innerHTML = user.email.charAt(0).toUpperCase();
  } else {
    hide(logoutBtn);
    show(loginBtn);
    hide(userInfo);
    email.innerHTML = "";
    avatar.innerHTML = "";
  }
  // console.log("setUser: ", user);
};

const showHideHandler = (data, el, states) =>
  states.includes(data.state) ? show(el) : hide(el);

const onLoad = () => {
  const logoutBtn = document.getElementById("auth-section__logout");
  const loginBtn = document.getElementById("auth-section__login");
  const cancelBtn = document.getElementById("cancel");
  chrome.storage.local.get(
    { processState: { ...processStateDefaults } },
    (data) => {
      currentData = data;
      setData(data.processState);
    }
  );
  cancelBtn.addEventListener("click", () => {
    chrome.runtime.sendMessage({
      type: "CRAFTA_ADPTER",
      action: "cancel",
      from: "popup",
    });
  });
  loginBtn.addEventListener("click", () => {
    const width = 985;
    const height = 735;
    const left = (screen.width - width) / 2;
    const top = (screen.height - height) / 4;
    window.open(
      "/widget.html",
      "Sign In",
      `width=${width},height=${height},top=${top},left=${left}`
    );
  });
  logoutBtn.addEventListener("click", () => {
    chrome.runtime.sendMessage({
      type: "CRAFTA_ADPTER",
      action: "logout",
      from: "popup",
    });
    // console.log("logout btn just pressed");
  });
  chrome.runtime.sendMessage({
    type: "CRAFTA_ADPTER",
    action: "user",
    from: "popup",
  });
};

document.addEventListener("DOMContentLoaded", onLoad);

const clockUpdate = (startTime) => {
  setTime(Date.now() - startTime);
};

const clockStart = (startTime) => {
  timerId = setInterval(clockUpdate, 1000, startTime);
  clockUpdate(startTime);
};

const clockStop = () => {
  clearInterval(timerId);
  timerId = null;
};

const setTime = (time) => {
  const date = new Date(time);
  const h = date.getUTCHours();
  const m = date.getUTCMinutes();
  const s = date.getSeconds();
  document.getElementById(
    "time"
  ).innerHTML = `Прошло: ${h} ч. ${m} мин. ${s} сек.`;
};

const hide = (el) =>
  el.classList.contains("hidden") || el.classList.add("hidden");

const show = (el) =>
  !el.classList.contains("hidden") || el.classList.remove("hidden");

const removeAllMessages = () =>
  (document.getElementById("messages").innerHTML = "");

const setMessage = (data) => {
  const stateInfo = states.find((s) => s.name === data.state);
  const type = stateInfo ? stateInfo.class : "info";
  removeAllMessages();
  if (Array.isArray(data.messages)) {
    data.messages.map((m) => addAlert(m.message, m.type));
  } else {
    addAlert(data.messages, type);
  }
};

const addAlert = (m, type = "info") => {
  const messages = document.getElementById("messages");
  let alert = document.createElement("div");
  alert.classList.add("alert", type);
  alert.innerHTML =
    m || "Здесь будет отображаться информация о работе Crafta adapter";
  messages.appendChild(alert);
};
