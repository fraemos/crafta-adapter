import "../css/content-crafta-products.css";
import {
  genBtn,
  init,
  buttons,
  getProducts,
  getSellers,
} from "./content/content-crafta";

let productSsrProps;

const getCurrentUrl = () => {
  const { protocol, host, pathname } = window.location;
  return `${protocol}//${host}${pathname}`;
};

const getSsrProps = (document, title) => {
  const props = document
    .querySelector(`[data-bazooka="${title}"]`)
    .getAttribute("data-ssr-props");
  if (!props) throw new Error("No ssr props");
  return JSON.parse(props);
};

const getProductSsrProps = (document) => {
  if (productSsrProps) return productSsrProps;
  productSsrProps = getSsrProps(document, "Product");
  return productSsrProps;
};

const getSellerUrl = (document) => {
  const parsed = getProductSsrProps(document);
  const value = parsed.memberUrl;
  if (!value) throw new Error("Url is absent or empty");
  return value;
};

const getBtnBuy = (document) => {
  return (
    document.querySelector('button[title="Добавить в корзину"]') ||
    document.querySelector('button[title="Додати в кошик"]') ||
    document.querySelector('meta[itemprop="availability"]')
  );
};

const initProductsPage = () => {
  try {
    const urls = [];
    const sellers = [];
    const btnBuy = getBtnBuy(document);
    if (btnBuy) {
      let url = getCurrentUrl();
      urls.push(url);
      let btnsWrap = document.createElement("div");
      btnsWrap.classList.add("ca-buttons-wrap");
      buttons.forEach((b) => btnsWrap.appendChild(genBtn(b, url)));
      btnBuy.parentNode.appendChild(btnsWrap);
      const seller = getSellerUrl(document);
      const btnSBl = document.querySelector(".ca-button-sbl");
      btnSBl.dataset.seller = seller;
      sellers.push(seller);
      // console.log("urls", urls);
      // console.log("seller", seller);
      init(urls);
      getProducts(urls);
      getSellers(sellers);
    }
  } catch (e) {
    console.error(e.message);
  }
};

initProductsPage();
