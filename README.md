# Crafta adapter

## Описание

Расширение для браузеров на основе Chromium, добавлящее дополнительный функционал для работы с сайтом [crafta.ua](https://crafta.ua/).

## Зависимости

- [node.js](https://nodejs.org)
- [native-client](https://github.com/andy-portmen/native-client)
- [git](https://git-scm.com/)
- [waifu2x-converter-cpp](https://github.com/DeadSix27/waifu2x-converter-cpp)

## Установка

* Установить node.js:
    * Создать новый файл .zshrc в домашней директории, если отсутствует (для macOS 10.15+):
        ```shell
        touch ~/.zshrc
        ```
    * Установить [nvm](https://github.com/nvm-sh/nvm)
    * Установить node.js через nvm:
        ```shell
        nvm install lts/erbium
        nvm use lts/erbium
        ```
* Установить [native-client](https://github.com/andy-portmen/native-client)
* Установить [git](https://git-scm.com/)
* Установить [Homebrew](https://brew.sh/)
* Установить `brew install moreutils jq`
* Установить `npm install --global yarn`
* Клонировать репозиторий `cd ~ && git clone https://gitlab.com/fraemos/crafta-adapter.git && cd ~/crafta-adapter`
* Выполнить `yarn && yarn run build`
* Установить расширене Crafta adapter (открыть [страницу управления расширениями](chrome://extensions/), включить режим разработчика и нажать "Загрузить распакованное расширение", выбрав дирепкторию `~/crafta-adapter/build`)
* Выполнить `chmod +x install.sh && ./install.sh`
* Установить [waifu2x-converter-cpp](https://github.com/DeadSix27/waifu2x-converter-cpp):
    ```shell
    brew install llvm opencv
    cd ~ && git clone https://github.com/DeadSix27/waifu2x-converter-cpp && cd waifu2x-converter-cpp
    cmake -DOPENCV_PREFIX=/usr/local/Cellar/opencv/<your version here> .
    make -j4
    sudo make install
    ```
* Для macOS 10.15+ отключить System Integrity Protection

## Обновление

- Выполинть команду `cd ~/crafta-adapter && git pull && yarn && yarn run build`
- Открыть [страницу управления расширениями](chrome://extensions/), для расширения Crafta adapter нажать кнопку обновить.

## Настройка

Расширене содержит настройки, позволяющие изменять некоторые параметры работы
